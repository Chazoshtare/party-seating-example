package people


import org.junit.jupiter.api.assertTimeout
import seatingproblem.Person
import seatingproblem.RelationshipGenerator
import java.time.Duration
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class RelationshipGeneratorTest {

    @Test
    fun `generates all possible combinations of relationships between people`() {
        // given
        val albert = Person("Albert", 30, 180)
        val bruce = Person("Bruce", 30, 180)
        val carlos = Person("Carlos", 30, 180)
        val dave = Person("Dave", 30, 180)
        val people = setOf(albert, bruce, carlos, dave)

        // when
        val relationships = RelationshipGenerator.generateRelationships(people)

        // then
        val expectedCombinations = setOf(
            setOf(albert, bruce),
            setOf(albert, carlos),
            setOf(albert, dave),
            setOf(bruce, carlos),
            setOf(bruce, dave),
            setOf(carlos, dave),
        )
        assertEquals(6, relationships.size)
        expectedCombinations.forEach {
            assertNotNull(relationships[it])
        }
    }

    @Test
    fun `generates million relationships in less that 1 second`() {
        // given
        val people = (1..1000).map { Person.generateRandom() }

        // when
        val relationshipProducer = { RelationshipGenerator.generateRelationships(people) }

        // then
        assertTimeout(Duration.ofSeconds(1)) {
            relationshipProducer.invoke()
        }
    }
}
