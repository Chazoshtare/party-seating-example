package people

import seatingproblem.Person
import kotlin.test.Test

class PersonTest {

    @Test
    fun `generates person with random name and surname`() {
        // given

        // when
        val person = Person.generateRandom()

        // then
        println(person)
    }
}
