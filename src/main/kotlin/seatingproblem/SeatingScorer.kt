package seatingproblem

object SeatingScorer {

    fun scoreLinearSeating(people: List<Person>, relationships: Relationships): Int =
        people.windowed(2) { relationships[it.toSet()]!! }.sum()

    fun scoreGroupSeating(people: List<Person>, relationships: Relationships, groupSize: Int): Int {
        // people sit in independent groups of [groupSize]
        // in each group, relationship between all people counts
        TODO()
    }
}
