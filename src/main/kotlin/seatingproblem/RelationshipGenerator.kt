package seatingproblem

import kotlin.random.Random
import kotlin.random.nextInt

typealias Relationships = Map<Set<Person>, Int>

object RelationshipGenerator {

    fun generateRelationships(people: Collection<Person>): Relationships {
        val relationships = mutableMapOf<Set<Person>, Int>()
        val peopleLeft = people.toMutableList()
        while (peopleLeft.isNotEmpty()) {
            val firstPerson = peopleLeft.removeFirst()
            peopleLeft.forEach {
                relationships[setOf(firstPerson, it)] = Random.nextInt(-100..100)
            }
        }
        return relationships.toMap()
    }
}
