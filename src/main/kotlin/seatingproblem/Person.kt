package seatingproblem

import kotlin.random.Random

data class Person(
    val fullName: String,
    val age: Int,
    val height: Int
) {

    companion object {
        private val VOWELS = setOf('a', 'e', 'i', 'o', 'u', 'y')
        private val CONSONANTS = ('b'..'z').toSet() - VOWELS

        fun generateRandom() = Person(
            generateRandomName(),
            Random.nextInt(18, 50),
            Random.nextInt(150, 200)
        )

        private fun generateRandomName(): String {
            val nameSyllableCount = Random.nextInt(2, 5)
            val surnameSyllableCount = Random.nextInt(2, 5)

            return buildString {
                repeat(nameSyllableCount) {
                    append("${CONSONANTS.random()}${VOWELS.random()}")
                }
                append(" ")
                repeat(surnameSyllableCount) {
                    append("${CONSONANTS.random()}${VOWELS.random()}")
                }
            }
        }
    }
}
