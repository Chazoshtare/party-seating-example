import seatingproblem.Person
import seatingproblem.RelationshipGenerator
import seatingproblem.SeatingScorer
import kotlin.system.measureTimeMillis

fun main(args: Array<String>) {
    val people = (1..11).map { Person.generateRandom() }
    val relationships = RelationshipGenerator.generateRelationships(people)

    val allCasesTime = measureTimeMillis {
        val allSeatings = generateAllListPermutations(people)
        println("All permutations size: ${allSeatings.size}")

        val bestSeating = allSeatings.map { seating ->
            seating to SeatingScorer.scoreLinearSeating(seating, relationships)
        }.maxBy { it.second }
        println("Best possible seating score: ${bestSeating.second}")
    }
    println("All cases analyzed in: ${allCasesTime}ms")
}
